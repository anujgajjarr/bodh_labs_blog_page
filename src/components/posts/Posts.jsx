import Post from "../post/Post";
import "./posts.css";

export default function Posts() {
  return (
    <div className="Container">
    <div class="tab">
  <button class="tablinks active" onclick="News(event, 'London')">Top</button>
  <button class="tablinks" onclick="News(event, 'Paris')">Recent</button>
  <button class="tablinks" onclick="News(event, 'Tokyo')">Citywise</button>
</div>

<div id="London" class="tabcontent">
  <h3>Top</h3>
  <p>London is the capital city of England.</p>
</div>

<div id="Paris" class="tabcontent">
  <h3>Recent</h3>
  <p>Paris is the capital of France.</p> 
</div>

<div id="Tokyo" class="tabcontent">
  <h3>Citywise</h3>
  <p>Tokyo is the capital of Japan.</p>
</div>


    <div className="sidebarItemm">
    <span className="sidebarTitlee">Indian Air Force</span>
    <hr></hr>
    <div className="posts">
    
      <Post img="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" />
      <Post img="https://images.pexels.com/photos/6758029/pexels-photo-6758029.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
      <Post img="https://images.pexels.com/photos/6711867/pexels-photo-6711867.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"/>
    </div>

      <div className="container-btn">
                    {/* <button className="btn d-flex justify-content-center align-items-center p-b-33" type="submit" >
                        <b>Get Otp</b>
                    </button> */}
                    <button type="submit" value="Submit" className="btn"><b>View All</b></button>
                </div>
    </div>
     
    

    <div className="sidebarItemm">
    <span className="sidebarTitlee">Sports</span>
    <hr></hr>
    <div className="posts">
    
      <Post img="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" />
      <Post img="https://images.pexels.com/photos/6758029/pexels-photo-6758029.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
      <Post img="https://images.pexels.com/photos/6711867/pexels-photo-6711867.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"/>
    </div>
    </div>
    <div className="container-btn">
                    {/* <button className="btn d-flex justify-content-center align-items-center p-b-33" type="submit" >
                        <b>Get Otp</b>
                    </button> */}
                    <button type="submit" value="Submit" className="btn"><b>View All</b></button>
                </div>
    


    <div className="sidebarItemm">
    <span className="sidebarTitlee">Health</span>
    <hr></hr>
    <div className="posts">
    
      <Post img="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" />
      <Post img="https://images.pexels.com/photos/6758029/pexels-photo-6758029.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
      <Post img="https://images.pexels.com/photos/6711867/pexels-photo-6711867.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"/>
    </div>
    </div>
    <div className="container-btn">
                    {/* <button className="btn d-flex justify-content-center align-items-center p-b-33" type="submit" >
                        <b>Get Otp</b>
                    </button> */}
                    <button type="submit" value="Submit" className="btn"><b>View All</b></button>
                </div>

    </div>




  );
}
